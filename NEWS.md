﻿# vaers 1.0.4

* Added installation notes for `vaersND` in Description field in DESCRIPTION file as pointed out by Uwe Ligges


# vaers 1.0.3

* Added < > around URLs in Description field in DESCRIPTION file as pointed out by Uwe Ligges


# vaers 1.0.2

* Updated source data


# vaers 1.0.1

* Updated source data


# vaers 1.0.0

* Initial release
